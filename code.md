
## how to implementing the print(np.array([1])) in np.array class ?


## Some Code Test Result:
>> a=[1,2,3] b=[1,2,3]  np.multiply(a,b) --> [1,4,9];  np.matmul(a,b) --> 14 np.matmul(a,b.T) --> 14
>> python in interactive mode, terminal will also print return value that define in a function


## To do:
    class global_var --> other file


## Q and A:
>> Q:ValueError: setting an array element with a sequence  
   A:  v[i] = w is wrong --> v[i] = w[i]

>>  Q:shape --> (2,)  represent that this shape is a transpose of (2) ?
   A:no, the shape of A and A.T is the same

>> Q:   how to cast superclass  numpy.ndarray to subclass in ?
   A:   np.array(...).view(myclass)

## Q and A(2):
>> how to translate a variable to a string as displayed in screen   ??
>> how to add a prefix for a call (like 'await f()'), so I can do something before this function's calling 
    --> this allows to setup a dynamic graph of a network 

>> how to reuse a memory in some function return ? such as 
    c=np.matmul in a function,
  Q;
   it will auto malloc a memory in np.matmul  then return to c?
   if yes, how to change it ?

  Maybe A:
    first , make the c as global
    then,look the address of c everytime invoking the function to decide if the memory is rellocated
         


## Noting:

__忘记测试diffprop改变的场景了(3.py)__

__init__.py存在的一个作用是映射，比如修改了库函数名，而不想修改源代码，可以使用其来进行remap函数名。
另一个是编译器来进行跳转查找。

已经测试，ndarray赋值给变量，dict,list的分量时，都不是索引拷贝，通过函数传递赋值也是如此，所以不用考虑由
拷贝引起的内存问题。  一个可能解释：由于python是动态的，传入什么(c层面)就解析成什么，传入参数如果是指针就当作指针处理，所以返回值也是指针。





