import numpy as np
class Tsarray(np.ndarray):
    """ derived from np.ndarray"""
    def __init__(self,shape,dtype=float,buffer=None,offset=0,strides=None,order=None):
        super(Tsarray,self).__init__(shape,dtype=float,buffer=None,offset=0,strides=None,order=None)
        self._init_Tsarray()

    def _init_Tsarray(self):
        self.id=1

    def __new__(cls,shape,dtype=float,buffer=None,offset=0,strides=None,order=None):
        return super(Tsarray,cls).__new__(cls,shape,dtype=float,buffer=None,offset=0,strides=None,order=None)
        # return 's' #the __new__ is to use to return any type instead of Tsarray

def array(ndarray_instance):
    ta = np.array(ndarray_instance,copy=False).view(Tsarray)
    ta._init_Tsarray()
    # ta = ndarray_instance
    # ta.__class__ = Tsarray  # will faile
    # ta._init_Tsarray()
    return ta

a=np.array([1,2])
b=array(a)

b[0] = 2
print(a)
print(b)
print(type(b))
print(b.id)

# class A():
#     def __init__(self):
#         print('A init')
#     def __new__(self):
#         print('A new')
# class B(A):
#     def __init__(self):
#         print('B init')
#     def __new__(cls):
#         super(B,cls).__new__(cls)
#         print('B new')

# b=B()

    