import numpy as np
class D(np.ndarray):
    def __init__(self,shape,dtype=float,buffer=None,offset=0,strides=None,order=None):
        print('a')
        self.a='1'

    def __new__(cls,shape,dtype=float,buffer=None,offset=0,strides=None,order=None):
        """
        You should consider that what you are trying to do is usually done with a Factory and that's the best way to do it. Using __new__ is not a good clean solution so please consider the usage of a factory. Here you have a good factory example.
        ref: https://stackoverflow.com/questions/674304/why-is-init-always-called-after-new
        """
        return super(D,cls).__new__(cls,shape,dtype=float,buffer=None,offset=0,strides=None,order=None)
        

d=D([1,2])
dd=D([2,1])
print(type(d))
print(d.T)
print(d.shape)
print(d.a)

np.matmul(d,dd)
nd=np.array(d)
print(nd)
print(nd.a)  # this will error, superclass has no 'a' attribute
