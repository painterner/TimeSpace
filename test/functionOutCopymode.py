import numpy as np

# global ga
# ga=None

a=[[1,2],[3,4]]
a=np.array(a)
b=[[1,2],[3,4]]
b=np.array(b)
def function():
    # global ga
    ga=np.matmul(a,b)
    return np.array(ga,copy=False)


# x=function()
# y=function()

ga=np.matmul(a,b)
x = np.array(ga,copy=False)

ga=np.matmul(a,b)
y = np.array(ga,copy=False)

x[0][0] = 3
print(x)
print(y)

## conclusion: x != y ,so matmul should auto malloc memory for result,
## how to reuse ga ?
