class A():
    def __init__(self):
        self.i=0
    def __iter__(self):    ## will invoke only once in one-time iteration operation
        self.i=0
        print('m')
        return self
    def __next__(self):
        self.i=self.i+1
        if(self.i>3):
            raise StopIteration
        return self.i
    def slice(self,start,stop,step):  ## built-in method is green, how to use ?
        pass



class B(A):
    def __init__(self):
        super(B)

a=B()
for i in a:
    print(i)

a.slice(0,2,1)

b=iter(a)
next(b)
