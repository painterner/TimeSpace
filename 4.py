#!/usr/bin/python3
#-*-coding:utf-8-*-

## bigstep  1 , normal program
## bigstep  2 , exploit Tensorflow Graph idea.

## bigstep 1:
      #step 1, forward 
      #step 2, backpropagation
      #step 3, optimize
      #    step 3.1, add midware in differential
      #    step 3.2, 
      #step 4, minist test
      #step 5, conv2d test
      #step 6, speech test (compare tensorflow sppeech sample)

import sys,os
import numpy as np
import Ts
from termcolor import colored

# print(colored('hello', 'red'), colored('world', 'green'))

forward_data=[]


ts = Ts.TS()
graph = Ts.graph()
tdo = Ts.data_ops.D_D_ops

def tsdebug(strs):
    sys.stdout.write(strs)
    sys.stdout.flush()


x=[[1,2,3],[3,4,5]]
x=np.array(x,dtype=np.float32)
y_=[5.0,7.0]
y_=np.array(y_).T
tsdebug('x\t'); print(x); tsdebug('y_\t'); print(y_)

w1= [1.0,1.0,1.0]
w1=np.array(w1,dtype=np.float32).T
b1= [1.0,1.0]
b1=np.array(b1,dtype=np.float32).T
tsdebug('w1 \t'); print(w1); tsdebug('b1 \t'); print(b1)

w2= [1.0,1.0]
b2= [1.0,1.0]
w2 = np.array(w2,dtype=np.float32).T
b2 = np.array(b2,dtype=np.float32).T

path=[]

def para_init():            # how to make sure the function was invoked only once ?
    if 'init' not in ISTARTER:
        ISTARTER['init'] = 1
        gb = global_var()
        gb.add_para('w1',w1)
        gb.add_para('b1',b1)
        gb.add_para('w2',w2)
        gb.add_para('b2',b2)

    return gb

def sepmer(x,gb_para_dict):       ## that is seperate and merge
    g = gb_para_dict
    # return x*g['w1'] + g['b1']
    # print('g[\'w1\']: shape: '+str(g['w1'].shape))  ## how to print numpy.arrary not in flatened mode ?
    tsdebug('sepmer w1\t'); print(g['w1'])
    # print('g[\'b1\']: shape: '+str(g['b1'].shape))
    tsdebug('sepmer b1'); print(g['b1'])
    
    

    # Y0 = np.matmul(x,g['w1'])+g['b1']
    Y0 = tdo.wx_b(x,w1,b1)
    
    # gnode1=graph.add_node(paraflag=True)

    # owb1=ts.jacobi_matrix(x,w1,Y0,bias=b1)


    

    # Y1 = np.multiply(Y0,g['w2'])+g['b2']
    # gnode2=graph.add_node(paraflag=True)
    Y1 = tdo.multiply_b(Y0,w2,b2)

    # owb12=ts.jacobi_matrix(Y0,w2,Y1,bias=b2,target=0)
    # owb2=ts.jacobi_matrix(Y0,w2,Y1,bias=b2)

    # owb1=np.matmul(owb12,owb1)
    
    # return Y1,owb1,owb2,[gnode1,gnode2]
    return Y1
def activate(sepmer,owb1,owb2,gnode):
    # act =  ts.sigmoid(sepmer)
    act = tdo.sigmoid(sepmer)
    # gnode=graph.add_node(gnode)

    # omid1 = ts.jacobi_sigmoid(act)
    # owb1 = np.matmul(omid1,owb1)
    # owb2 = np.matmul(omid1,owb2)
    # act = sepmer
    tsdebug('activate:\t'); print(act)
    # return act,owb1,owb2,gnode
    return act

def forward(activate):
    XW1 = tdo.input([x,w1])
    B1 = tdo.input(b1)

    T0 = tdo.xw_b(XW1[0],XW1[1],B1[0])
    T1 = tdo.multiply_b(T0,w2,b2)
    T2 = tdo.sigmoid(T1)

    ry = tdo.normalize_y_(y)
    ry = t2 - ry
    T3 = tdo.power(ry,2)
    T3 = tdo.reduce(T3)

    
    # print('shape activate: '+str(activate.shape))
    tsdebug('forward:\t'); print(activate)
    return activate


def cross_loss(forward,y,owb1,owb2,gnode):                         ## forward
    if(forward.shape != y.shape):
        print("prediction\'s shaped is not equal to label\'s")
        exit()
    ry = ts.normalize_y_(y)
    # ry = ts.sigmoid(y)
    tsdebug('cross_compare label\t');print(ry)
    ry = forward - ry
    # r = np.power(ry,2)
    r = Ts.power(ry,2)

    r = ts.reduce(r)

    # gnode=graph.add_node(gnode)

    # omid2 = ts.jacobi_std_vector(ry)
    # owb1 = np.matmul(omid2,ry)
    # owb2 = np.matmul(omid2,ry)
    tsdebug('cross_compare r\t'); print(r)
    # return r,owb1,owb2,gnode
    return r


# def differential(diff_variable,diff_function,midware=None):  ## back differential
#     II = 1  
#     if(midware==None):
#         diff_last = diff_function/diff_variable 
#     else:
#         ss = len(midware)                        
#         for i in range(ss):
#             diff=diff_function/midware[i]
#             II = diff * II   
#         diff_last = midware[-1]/diff_variable
#     II = diff_last * II
    
#     tsdebug('differential:\t'); print(II)
#     return II


def backpropagation(target):
    for idx in target.pres:
        cur_data=Ts.global_var.default_graph.data[idx]
        curF= getFunction(cur_data.method[0])
        
        curJacobi=curF(method[1:])
        curJacobi=np.matul(target.JacobiData,curJacobi)  ## target.JacobiData to set 对角为1阵。 >>??
    
        reJa=reshape(curJacobi,cur_data.shape)
        if(cur_data.pres==[]):
            cur_data.backData=cur_data-reJa        
        else:
            cur_data=cur_data-reJa   ## let forward to fetch  >> ??
            cur_data.jacobiData = reJa
        
        Ts.global_var.default_graph.set_tsarray(cur_data)

        # call self recursive
        backpropagation(cur_data)



# def backpropagation(loss,gb,owb1,owb2):
#     array=[owb1,owb2]
#     for iname, iindex, owb in zip(gb,array):
#         WB=gb.get_para_by_name(iname)
#         WB=WB-owb
#         gb.set_para_by_name(iname,WB)

        

def main():
    gb=para_init()
    
    sep,owb1,owb2 = sepmer(x,gb.para_dict)
    act,owb1,owb2 = activate(sep,owb1,owb2)
    fact = forward(act)
    loss,owb1,owb2 = cross_loss(fact,y_,owb1,owb2)

    backpropagation(loss,gb,owb1,owb2)


