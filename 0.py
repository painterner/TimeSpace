#!/usr/bin/python3
#-*-coding:utf-8-*-

## bigstep  1 , normal program
## bigstep  2 , exploit Tensorflow Graph idea.

## bigstep 1:
      #step 1, forward 
      #step 2, backpropagation

import sys,os
import math
import numpy as np

ISTARTER = {}

x=[[1,2,1],[3,4,3]]
y_=[[5,6],[7,8],[9,10]]


w1= 1
b1= 1

class global_var():
    def __init__(self):
        self.para_list = []
        self.para_dict = {}
        self.para_list_len = None
        self.para_current = 0
    def add_para(self,name,para):
        self.para_list.append(name)
        self.para_dict[name] = para
        self.para_list_len = len(self.para_list)

    def __iter__(self):
        return self
    def __next__(self):  # python 2 will be next() ?
        if(self.para_current >= self.para_list_len):  
            self.para_current = 0                       ## this is safe
            raise StopIteration
        self.para_current = self.para_current + 1
        return self.para_list[self.para_current - 1] ,self.para_current - 1

    @property
    def get_para_size(self):
        return len(self.para_list)

    @property
    def get_para_dict(self):
        return self.para_list

    # @property                         ## property not callable
    def get_para_by_name(self,name):
        """ get para by name """
        return self.para_dict[name]

    # @property
    def get_para_by_index(self,index):
        return self.para_dict[self.para_list[index]]

    def set_para_by_name(self,name,value):
        self.para_dict[name] = value

    def set_para_by_index(self,index,value):
        self.para_dict[index] = value

def para_init():            # how to make sure the function was invoked only once ?
    if 'init' not in ISTARTER:
        ISTARTER['init'] = 1
        gb = global_var()
        gb.add_para('w1',w1)
        gb.add_para('b1',b1)

    return gb

def sepmer(x,gb_para_dict):       ## that is seperate and merge
    g = gb_para_dict
    return x*g['w1'] + g['b1']

def activate(sepmer):
    return math.sin(sepmer)

def forward(activate):
    r=activate(sep)

    return r

def compare(forward,y):                         ## forward
    return forward-y


def differential(diff_variable,diff_function,midware=None):  ## back differential
    ss = len(midware)
    II = 1                          
    for i in range(ss):
        diff=diff_function/midware[i]
        II = diff * II

    if(midware==None):
        diff_last = diff_function/diff_variable 
    else:   
        diff_last = midware[-1]/diff_variable
    II = diff_last * II
    
    return II


def backpropagation(compare,diff,gb):
    for i in range(compare):
        diff_ = differential(diff,compare[i][1]-compare[i][0])
        v = gb.get_para_by_index[i]-diff_
        gb.set_para_by_index(i,v)


gb=para_init()

forwd=np.zeros([2])
comp = np.zeros([2,2])
diff = 0.2
for iname, iindex in gb:
    for i in range(2):
        lastv=gb.get_para_by_name(iname)
        gb.set_para_by_name(iname,lastv+diff)

        sep = sepmer(x,gb.para_dict)
        act = activate(sep)
        forwd[i] = forward(act)
        comp[iindex][i] = compare(forwd[i],y_)

backpropagation(comp,diff,gb)


