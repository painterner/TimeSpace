
# History:
__2018.8.25 evening__
    input: x= [[1,2,3],[3,4,5]] w1=[1,1,1] b=[1,1] y_[5,7]

    w1[ 0.9758244   0.96667643 -1.44080875]
    b1[0.99535087 0.99356267]
    sepmer w1       [ 0.9758244   0.96667643 -1.44080875]
    sepmer b1[0.99535087 0.99356267]
    activate:       [-0.41789812  0.58369784]
    activate type:  [-0.41789812  0.58369784]
    forward:        [-0.41789812  0.58369784]
    cross_compare   [0.41666667 0.58333333]
    cross_compare   0.1663661917009449
    cross_error
    0.1663661917009449

by some steps of training to calcute y[0]
    1*0.9758244+2*0.96667643+3*(-1.44080875)+0.99535087
    -.41789812   --> y[0] 
__结果与y_[0] = 5 相差甚远，应该是激活函数，归一化等设置有问题。__

__2018.8.26  am__
    3.py  --> y->simoid  y_ simoid  then use the variance(方差)
    train_step = 10000  seg_n = 1000 study_rate = 0.1

    forward:        [0.99136781 0.99986492]
    cross_compare label     [0.99330715 0.99908895]
    cross_compare r 4.363173384130042e-06
    cross_error
    4.363173384130042e-06

__反向计算 forward --> (4.74，8.91)__
    (观察到如果seg_n=100,则最终偏移会越来越大)

    3.py  --> y-> sigmoid y_ normalize  then use the varicance
    tran_step = n   seg_n = m study_rate = 10

    n = 500  n = 24  variance = 0.008
        600      30             0.0006
        700      37             0.0005
    ....
        1400     90             0.0005

    when n = 700
    forward:        [0.39576362 0.58960586]
    cross_compare label     [0.41666667 0.58333333]
    cross_compare r 0.00047628183090566936
    cross_error
    0.00047628183090566936
    
__反向计算 forward --> (4.74，7.07)__

