import sys,os
import numpy as np 
import numpy.matlib as npmb

if(__name__=='main'):
    from Ts.sub.global_data import global_data
    from Ts.sub.graph import graph,graph_node
else:
    from sub import global_data
    from sub import graph


class Tsarray(np.ndarray):
    """ derived from np.ndarray"""
    def __init__(self,shape,dtype=float,buffer=None,offset=0,strides=None,order=None):
        super(Tsarray,self).__init__(shape,dtype=float,buffer=None,offset=0,strides=None,order=None)
        self._init_Tsarray()

    def _init_Tsarray(self):
        self.id=None
        self.method=[]
        self.pres=[]
        self.jacobiData=None
        self.backData=None

    def __new__(cls,shape,dtype=float,buffer=None,offset=0,strides=None,order=None):
        return super(Tsarray,cls).__new__(cls,shape,dtype=float,buffer=None,offset=0,strides=None,order=None)
        # return 's' #the __new__ is to use to return any type instead of Tsarray

def array(ndarray_instance):
    tsarray = np.array(ndarray_instance,copy=False).view(Tsarray)
    tsarray._init_Tsarray()
    return tsarray


class TS():
#     def sigmoid(self,x):
#         return 1/(1+np.exp(-x))

    def normalize_y_(self,y_):
        E=0
        y=y_
        for x in y:
            E=E+x
        for i in range(len(y)):
            y[i] = y[i]/E
        return y

    def reduce(self,x):
        E = 0
        for i in x:
            E = i+E
        return E

    def np_platLen(self,A):
        """ input: a ndarray """
        II = 1
        for x in A.shape:
            II = x*II
        return II

    def np_axesSize(self,A):
        for count,_ in enumerate(A):
            pass
        return count+1

    def jacobi_bias(self,bias,Y):
        rows = self.np_platLen(Y)
        cols = self.np_platLen(bias)
        if(cols != rows):
            raise Exception("bias_len must equal rows")

        jacobi=np.zeros(shape=[rows,cols],dtype=np.float32)
        for row in rows:
            jacobi[row][row] = 1.0

        return jacobi


    def jacobi_matrix(self,A,B,Y,target=1):
        """ input A,B, output Y; MUST --> AB = Y
            optimized target default 1. if 0, means to set A as variable; if 1, means
        set B as variable 
        returns: jacobi_ndarray
        """
        rows = self.np_platLen(Y)
        bias_len = 0
        cols = bias_len
        if(A.shape[1] != B.shape[0] ):
            raise Exception("A.shape[1] must be equal to B.shape[0]")
        
        if(target ==  0):
            cols = self.np_platLen(A) + cols
            jacobi = np.zeros(shape=[rows,cols],dtype=np.float32)
            for i in range(A.shape[0]):
                for ii in range(B.shape[1]):
                    for iii in range(A.shape[1]):
                        jacobi[i*B.shape[0]+iii] = B[iii][ii]


        elif(target == 1):
            cols = self.np_platLen(B) + cols
            jacobi = np.zeros(shape=[rows,cols],dtype=np.float32)
            # build jacobi, maybe we can use some parallel method or use numpy lib
            for i in range(A.shape[0]):
                for ii in range(B.shape[1]):
                    for iii in range(A.shape[1]):
                        jacobi[ii+iii*B.shape[1]] = A[i][iii]

             
        return jacobi



    def jacobi_matrix_b(self,A,B,Y,bias,target=1):
        """ input A,B, output Y; MUST --> AB = Y
            optimized target default 1. if 0, means to set A as variable; if 1, means
        set B as variable 
        returns: jacobi_ndarray
        """
        rows = self.np_platLen(Y)
        bias_len = self.np_platLen(bias)
        cols = bias_len
        if(A.shape[1] != B.shape[0] ):
            raise Exception("A.shape[1] must be equal to B.shape[0]")
        
        if(target ==  0):
            cols = self.np_platLen(A) + cols
            jacobi = np.zeros(shape=[rows,cols],dtype=np.float32)
            for i in range(A.shape[0]):
                for ii in range(B.shape[1]):
                    for iii in range(A.shape[1]):
                        jacobi[i*B.shape[0]+iii] = B[iii][ii]


        elif(target == 1):
            cols = self.np_platLen(B) + cols
            jacobi = np.zeros(shape=[rows,cols],dtype=np.float32)
            # build jacobi, maybe we can use some parallel method or use numpy lib
            for i in range(A.shape[0]):
                for ii in range(B.shape[1]):
                    for iii in range(A.shape[1]):
                        jacobi[ii+iii*B.shape[1]] = A[i][iii]

        
        ## fill bias
        end = cols-bias_len
        if(bias_len == rows):
            for i in range(bias_len):
                jacobi[i,end+i] = 1.0
        else:
            raise Exception("bias_len must equal rows")
                
        return jacobi

    def jacobi_sigmoid(self,Y):
        rows=self.np_platLen(Y)
        Y=np.multiply(1-Y,Y)
        Y=np.reshape(Y,newshape=[rows,])
        jacobi=np.zeros(shape=(rows,rows),dtype=np.float32)

        # sigmoad differential is a diagonal matrix
        for row in range(rows):
            jacobi[row][row] = Y[row]

        return jacobi

    def jacobi_std_vector(self,C):
        """ jacobi matrix of making std(A)"""
        if(self.np_axesSize(A) != 1 ):
            print('input must be a vector')

        return 2*C
