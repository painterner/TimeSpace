import sys,os
from Ts.global_var import acquireCountGnodeId,getCountGnodeId,clearCountGnodeId

class Graph_Node():
    def __init__(self,pres=None,paraflag=False):
        """paraflag --> if true,this node has paras to be optimized"""
        self.pres=pres
        self.paraflag=True

# # if node is not value copy
# class Graph():
#     def __init__(self):
#         self.built=False
#     def add_tsarray(self,v,pres):
#         # if(not self.built):
#         v.id = acquireCurGnodeId()
#         v.pres = pres

#         return v

#     def build(self):
#         self.built=True


# if node is value copy,we build a new data dict to store it
class Graph():
    def __init__(self):
        self.built=False
        self.data={}
        self.idlength=None
        self.cur_id=-1
    def add_tsarray(self,v,pres):
        # if(not self.built):
        tsid=acquireCountGnodeId()
        # v.id=str(tsid)
        # self.data[v.id] = v
        idx=str(tsid)
        v.id=idx
        self.data[idx] = v
        v.pres = [i.id for i in pres]
        return v
        # else:
        #     self.data[v.id] = v

    def get_tsarray(self):
        # cur_id = getCountGnodeId()
        self.cur_id = self.cur_id + 1
        return self.cur_id

    def set_tsarray(self,data):
        self.data[data.id] = data

    def build(self):
        self.built=True
        self.idlength=getCountGnodeId()
        clearCountGnodeId()


            
# class graph_node():
#     def __init__(self,pres=None,paraflag=False):
#         """paraflag --> if true,this node has paras to be optimized"""
#         self.pres=pres
#         self.paraflag=True


# class graph():
#     def __init__(self):
#         self.node_chain=[]
#         self.layer=0

#     def check_layer(self,layer):
#         if(layer > self.layer):
#             return True
#         return False

#     def add_node(self,layer,pres=None,paraflag=False):
#         """input    
#             layer: self.nodes first dimension
#             inode: self.nodes second dimension
#             pres: graph_node array"""
#         if(self.check_layer(layer))
#             self.node_chain=[]
#         gnode = graph_node(paraflag)
#         self.node_chain.append(gnode)
#         self.node_chain[-1].pres=pres

#         return self.node_chain

