import numpy as np

class global_data():
    def __init__(self):
        self.para_dict = {}
        self.jacobi_dict = {}

        self.para_len = 0
    def add_para(self,name,para):
        self.para_dict[name] = para
        self.para_len = self.para_len + 1

    def append_para(self,input):
        ## input --> string 'input' ++ add_para
        name=None
        self.add_para(name,input)

    def iter(self):
        for key in self.para_dict.keys():
            yield self.para_dict[key], self.jacobi_dict[key]

    @property
    def get_para_size(self):            ## you can do some logic (such as: error notification ability) before return the value
        return len(self.para_len)

    @property
    def get_para_dict(self):
        return self.para_dict

    @property
    def get_jacobi_dict(self):
        return self.jacobi_dict

    # @property                         ## property not callable
    def get_para_by_name(self,name):
        """ get para by name """
        return self.para_dict[name]

    def set_para_by_name(self,name,value):
        self.para_dict[name] = value

