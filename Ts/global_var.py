import Ts
# this is project-wide Variable
global cur_Gnode_Id
cur_Gnode_Id=-1
def acquireCountGnodeId():
    global cur_Gnode_Id
    cur_Gnode_Id = cur_Gnode_Id + 1
    return cur_Gnode_Id

def getCountGnodeId():
    global cur_Gnode_Id
    return cur_Gnode_Id

def clearCountGnodeId():
    global cur_Gnode_Id
    cur_Gnode_Id=-1

default_graph=Ts.Graph()