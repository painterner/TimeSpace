import numpy as np
# from Ts.ts import Tsarray
# from Ts.sub.graph import Graph
# import Ts.sub.graph as graph
import Ts

class Data_Ops():

    def input(self,inputs):
        l = len(inputs)
        tsarray=[]
        for i in range(l):
            if(Ts.global_var.default_graph.built):
                idx=Ts.global_var.default_graph.get_tsarray()
                # we predict that need next data
                inputs[i]=Ts.global_var.default_graph.data[str(idx+1)]
            tsarray.append(Ts.array(inputs[i]))
            tsarray[i]=Ts.global_var.default_graph.add_tsarray(tsarray[i],[])
        
        return tsarray

    def matmul(self,x,w,out=None):
        cal=[w]

        narray=np.matmul(x,w)

        tsarray=Ts.array(narray)
        v=Ts.global_var.default_graph.add_tsarray(tsarray,cal)
        return v

    def xw_b(self,x,w,bias):
        cal=[w,bias]
       
        narray=np.matmul(x,w)+bias

        tsarray=Ts.array(narray)
        v=Ts.global_var.default_graph.add_tsarray(tsarray,cal)
        x.method=[0,v.id,w.id]      ## [function id,afters,pres]
        w.method=[0,v.id,x.id]
        bias.method = [0,v.id,x.id]

        return v

    def multiply(self,x,w):
        narray = np.multiply(x,w)

        tsarray = Ts.array(narray)
        v=Ts.global_var.default_graph.add_tsarray(tsarray,[x,w])
        return v

    def multiply_xwb(self,x,w,bias):
        narray = np.multiply(x,w)

        tsarray = Ts.array(narray)
        v=Ts.global_var.default_graph.add_tsarray(tsarray,[x,w,bias])
        return v

    def sigmoid(self,a):
        narray=1/(1+np.exp(-a))

        tsarray = Ts.array(narray)
        v=Ts.global_var.default_graph.add_tsarray(tsarray,[a])
        return v

    def power(self,base,index):
        narray = np.power(base,index)

        tsarray = Ts.array(narray)
        v=Ts.global_var.default_graph.add_tsarray(tsarray,[base])
        return v

D_D_ops=Data_Ops()