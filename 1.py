#!/usr/bin/python3
#-*-coding:utf-8-*-

## bigstep  1 , normal program
## bigstep  2 , exploit Tensorflow Graph idea.

## bigstep 1:
      #step 1, forward 
      #step 2, backpropagation

import sys,os
import numpy as np

def tsdebug(strs):
    sys.stdout.write(strs)
    sys.stdout.flush()

ISTARTER = {}

x=[[1,2,1],[3,4,3]]
x=np.array(x,dtype=np.float32)
y_=[5.0,7.0]
y_=np.array(y_).T
tsdebug('x\t'); print(x); tsdebug('y_\t'); print(y_)

w1= [1.0,1.0,1.0]
w1=np.array(w1,dtype=np.float32).T
b1= [1.0,1.0]
b1=np.array(b1,dtype=np.float32).T
tsdebug('w1 \t'); print(w1); tsdebug('b1 \t'); print(b1)

class global_var():
    def __init__(self):
        self.para_list = []
        self.para_dict = {}
        self.para_list_len = None
        self.para_current = 0
    def add_para(self,name,para):
        self.para_list.append(name)
        self.para_dict[name] = para
        self.para_list_len = len(self.para_list)

    def __iter__(self):
        return self
    def __next__(self):  # python 2 will be next() ?
        if(self.para_current >= self.para_list_len):  
            self.para_current = 0                       ## this is safe
            raise StopIteration
        self.para_current = self.para_current + 1
        return self.para_list[self.para_current - 1] ,self.para_current - 1

    @property
    def get_para_size(self):
        return len(self.para_list)

    @property
    def get_para_dict(self):
        return self.para_list

    # @property                         ## property not callable
    def get_para_by_name(self,name):
        """ get para by name """
        return self.para_dict[name]

    # @property
    def get_para_by_index(self,index):
        return self.para_dict[self.para_list[index]]

    def set_para_by_name(self,name,value):
        self.para_dict[name] = value

    def set_para_by_index(self,index,value):
        self.para_dict[self.para_list[index]] = value
        

def para_init():            # how to make sure the function was invoked only once ?
    if 'init' not in ISTARTER:
        ISTARTER['init'] = 1
        gb = global_var()
        gb.add_para('w1',w1)
        gb.add_para('b1',b1)

    return gb

def sepmer(x,gb_para_dict):       ## that is seperate and merge
    g = gb_para_dict
    # return x*g['w1'] + g['b1']
    # print('g[\'w1\']: shape: '+str(g['w1'].shape))  ## how to print numpy.arrary not in flatened mode ?
    tsdebug('sepmer w1\t'); print(g['w1'])
    # print('g[\'b1\']: shape: '+str(g['b1'].shape))
    tsdebug('sepmer b1'); print(g['b1'])
    
    return np.matmul(x,g['w1'])+g['b1']

def activate(sepmer):
    act =  np.sin(sepmer)
    tsdebug('activate:\t'); print(act)
    return act

def forward(activate):
    
    # print('shape activate: '+str(activate.shape))
    tsdebug('forward:\t'); print(activate)
    return activate

def cross_compare(forward,y):                         ## forward
    if(forward.shape != y.shape):
        print("prediction\'s shaped is not equal to label\'s")
        exit()

    r = np.matmul(forward,y)
    tsdebug('cross_compare\t'); print(r)
    return r


def differential(diff_variable,diff_function,midware=None):  ## back differential
    II = 1  
    if(midware==None):
        diff_last = diff_function/diff_variable 
    else:
        ss = len(midware)                        
        for i in range(ss):
            diff=diff_function/midware[i]
            II = diff * II   
        diff_last = midware[-1]/diff_variable
    II = diff_last * II
    
    tsdebug('differential:\t'); print(II)
    return II


def backpropagation(compare,gb):
    for ii in range(len(compare)):
        v=None
        slen = len((gb.get_para_by_index(ii)))
        for i in range(slen):
            diff_ = differential(compare[ii][i],compare[ii][i][1]-compare[ii][i][0])
            v = np.array(gb.get_para_by_index(ii),copy=True)
            tsdebug('backpropagation: v s1\t'); print(v); 
            tsdebug('backpropagation: diff_ s1\t'); print(diff_); 
            w = v-diff_[:slen]
            v[i] = w[i]
        
        tsdebug('backpropagation: v s2\t'); print(v)
        gb.set_para_by_index(ii,v)

gb=para_init()

# forwd=np.zeros([2,2],dtype=np.float32)
# forwd=forwd.T
comp = np.zeros([2,3,3],dtype=np.float32)  ## bi in second dimention is 2, but w1 is 3 ,so use 3 is enouph
# print('comp shape: '+str(comp.shape))
diffprop = 0.2
count = 0
for iname, iindex in gb:
    print('gb.para_dict: '+str(gb.para_dict[iname])+' name: '+iname)
    for isindex in range(len(gb.para_dict[iname])): 
        count = count + 1
        lastv=gb.get_para_by_name(iname)  
        print('iindex {}, isindex {},lastv {}'.format(iindex,isindex,lastv))
        comp[iindex][isindex][2] = diffprop*lastv[isindex] 
        for i in range(2):
            if(i==0):
                slastv = np.array(lastv,copy=True)
                slastv[isindex] = lastv[isindex] + diffprop*lastv[isindex]
                print('slavtv: '+str(slastv))
                gb.set_para_by_name(iname,slastv)
            else:
                gb.set_para_by_name(iname,lastv)

            sep = sepmer(x,gb.para_dict)
            act = activate(sep)
            # print('forwd ')
            # print(forwd)
            fact = forward(act)
            print('fact')
            print(fact)
            comp[iindex][isindex][1-i] = cross_compare(fact,y_)  ## 1-D: w1,b1;  2-D: w11,w12; 3-D: diffw111 diffw112
        print('\n\ncount:{}'.format(count))

backpropagation(comp,gb)




for iname,iindex in gb:
    tsdebug(iname); print(gb.get_para_by_name(iname))

